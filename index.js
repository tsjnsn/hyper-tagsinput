import nestable from 'hyperapp-nestable'

const BACKSPACE = 8
const TAB = 9
const ENTER = 13
const LEFT = 37
const RIGHT = 39
const DELETE = 46
const COMMA = 188

const TAG_MARGIN = 5

function caretAtStart (el) {
  try {
    return el.selectionStart === 0 && el.selectionEnd === 0
  } catch (e) {
    return el.value === ''
  }
}

export default nestable(
  {
    value: null,
    baseWidth: 0,
    width: 0,
    height: null,
    selected: null,
    tags: [],
    atStart: false,
    hiddenTags: [],
    takeFocus: false,
    isFocused: false,
    tagOffsets: [],
    tagWidths: [],
    scrollLeft: 0
  },

  {
    init: props => ({value: props.value || 'test'}),
    set: newValue => newValue,
    deleteTag: ({tagIndex, onTagRemoved}) => (state, actions) => {
      try {
        onTagRemoved && onTagRemoved({tags: state.tags, value: state.tags[tagIndex], index: tagIndex})
      } catch (ex) {
        console.log(ex)
      }
      const tags = [ ...state.tags ]
      tags.splice(tagIndex, 1)
      if (tags.length === 0) actions.unselect()
      return { tags }
    },
    savePartialInput: value => (state, actions) => {
      if (!value) return
      return {hiddenTags: [...state.hiddenTags, value], value: null, takeFocus: true, atStart: true}
    },
    setTagData: ({index, offset, width}) => state => {
      const tagOffsets = [...state.tagOffsets]
      const tagWidths = [...state.tagWidths]
      tagOffsets[index] = offset
      tagWidths[index] = width
      return { tagOffsets, tagWidths }
    },
    setInputWidth: width => state => {
      return { width }
    },
    setInputHeight: height => state => {
      return { height }
    },
    tagAdded: ({ev, value, onTagAdded}) => (state, actions) => {
      actions.setInputWidth(state.width + ev.offsetWidth + TAG_MARGIN)
      const tags = [...state.tags, value]
      try {
        onTagAdded && onTagAdded({tags, value})
      } catch (ex) {
        console.log(ex)
      }
      actions.set({scrollLeft: ev.offsetLeft + ev.offsetWidth})
      return { tags, hiddenTags: state.hiddenTags.filter(x => x !== value) }
    },
    tagRemoved: (ev) => (state, actions) => {
      actions.setInputWidth(state.width - ev.offsetWidth - TAG_MARGIN)
    },
    fixScroll: (selected) => (state, actions) => {
      const leftSide = state.tagOffsets[selected]
      const width = state.tagWidths[selected]
      const rightSide = leftSide + width
      const tagListWidth = Math.min(state.width, state.baseWidth * 0.75)
      if (tagListWidth + state.scrollLeft < rightSide) {
        actions.set({scrollLeft: rightSide - tagListWidth + TAG_MARGIN})
      }
      if (state.scrollLeft > leftSide) {
        actions.set({scrollLeft: leftSide})
      }
    },
    selectLast: _ => state => {
      if (state.tags.length === 0) return
      return { selected: state.tags.length - 1 }
    },
    selectNext: scrollEl => (state, actions) => {
      if (state.selected + 1 === state.tags.length) {
        actions.unselect()
        return
      }
      actions.fixScroll(state.selected + 1)
      return { selected: state.selected + 1 }
    },
    selectPrevious: scrollEl => (state, actions) => {
      if (state.selected === 0) return
      actions.fixScroll(state.selected - 1)
      return { selected: state.selected - 1 }
    },
    unselect: _ => state => {
      if (state.selected === null) return
      return { selected: null }
    }
  },

  (state, actions) => (props, children) => {
    const InputElement = props.element || ((props, children) => <input {...props} />)
    return <div style={{position: 'relative'}}>
      <InputElement {...props} value='' tabIndex='-1' style={{userSelect: 'none', position: 'absolute', background: 'transparent'}}
        onfocus={() => actions.set({takeFocus: true, isFocused: true})}
        oncreate={(ev) => {
          window.addEventListener('resize', () => {
            actions.set({baseWidth: ev.offsetWidth - TAG_MARGIN})
          })
          actions.set({baseWidth: ev.offsetWidth - TAG_MARGIN})
        }}
      />
      <div
        style={{
          overflowX: 'auto',
          whiteSpace: 'nowrap'
        }}
      >
        <div
          style={{display: 'inline-block', maxWidth: '75%', overflowX: 'auto', whiteSpace: 'nowrap'}}
          oncreate={(ev) => { ev.scrollLeft = state.scrollLeft }}
          onupdate={(ev) => { ev.scrollLeft = state.scrollLeft }}
        >
          {state.tags.map((value, index) => _ =>
            <span
              style={{height: state.height, margin: '5px 0px 5px 5px'}}
              class={`tag ${state.isFocused && index === state.selected && 'is-info'}`}
              key={`${index}-${value}`}
              oncreate={(ev) => { actions.setTagData({index, offset: ev.offsetLeft - TAG_MARGIN, width: ev.offsetWidth}) }}
              ondestroy={(ev) => actions.tagRemoved(ev)}
            >{value}</span>
          )}
          {state.hiddenTags.map(value => _ =>
            <span
              class='tag'
              style={{marginLeft: `${TAG_MARGIN}px`, marginBottom: '5px', marginTop: '5px', height: state.height + 'px', visibility: 'hidden', position: 'absolute'}}
              oncreate={(ev) => actions.tagAdded({ev, value, onTagAdded: props.onTagAdded})}
            >{value}</span>
          )}
        </div>
        <InputElement
          key='tags-input-main'
          style={{
            background: 'transparent',
            // background: '#fcfcfc',
            border: 'none',
            boxShadow: 'none',
            width: Math.max(state.baseWidth - state.width, state.baseWidth * 0.25) + 'px' || '',
            textShadow: state.selected !== null ? '0 0 0 lightgray' : '',
            color: state.selected !== null ? 'transparent' : ''
          }}
          onupdate={(ev) => {
            state.takeFocus && ev.focus()
          // state.width > 0 || actions.setInputWidth(0)
          // state.height > 0 || actions.setInputHeight(-10)
          }}
          onblur={(ev) => {
            actions.set({isFocused: false, takeFocus: false})
          }}
          onfocus={(ev) => {
            actions.set({isFocused: true, takeFocus: true})
          }}
          onclick={(ev) => {
            actions.unselect()
          }}
          onkeydown={(ev) => {
            const key = ev.keyCode || ev.which
            const atStart = caretAtStart(ev.target)
            const isAnySelected = state.selected !== null
            if (key === ENTER || key === COMMA || key === TAB) {
              if (!ev.target.value && key !== COMMA) return
              actions.savePartialInput(ev.target.value)
              ev.target.value = ''
              ev.preventDefault()
            } else if (key === DELETE && isAnySelected) {
              actions.deleteTag({tagIndex: state.selected, onTagRemoved: props.onTagRemoved})
            } else if (key === BACKSPACE) {
              if (isAnySelected) {
                actions.selectPrevious(ev.target.parentNode)
                actions.deleteTag({tagIndex: state.selected, onTagRemoved: props.onTagRemoved})
              }
              if (!isAnySelected && atStart) {
                actions.selectLast()
              }
            } else if (key === LEFT) {
              if (isAnySelected) {
                actions.selectPrevious(ev.target.parentNode)
              } else if (!atStart) {

              } else {
                actions.selectLast()
              }
            } else if (key === RIGHT) {
              if (!isAnySelected) return
              actions.selectNext(ev.target.parentNode)
              ev.preventDefault()
            } else {
              actions.unselect()
            }
          }}
        />
      </div>
    </div>
  }
)
