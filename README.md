# hyper-tagsinput

```js
import { HyperTagsInput } from 'hyper-tagsinput'
<HyperTagsInput element={input}
  onTagAdded={({tags, value}) => console.log({tags, value})}
  onTagRemoved={({tags, value}) => console.log({tags, value})} />
```
